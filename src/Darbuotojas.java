import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Darbuotojas extends Database {

    private int id;
    private String name;
    private String surname;
    private String gender = "vyras";
    private String phone = "112";
    private String birthday = "2019-05-14 17:10:07";
    private String education = "Vidurinis";
    private Double salary = 0.0;
    private String pareigos = "direktorius";

    Darbuotojas(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    Darbuotojas(int id, String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.id = id;
    }

    public String getFullName() {
        return this.name + " " + this.surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Darbuotojas> getAll() {
        List<Darbuotojas> temp = new ArrayList<>();

        try {
            String selectQueryStatement = "SELECT * from darbuotojai";

            databasePrepareStatement = databaseConnection.prepareStatement(selectQueryStatement, Statement.RETURN_GENERATED_KEYS);
            ResultSet generatedKeys = databasePrepareStatement.executeQuery();

            while (generatedKeys.next()) {

                String nameDB = generatedKeys.getString("name");
                String surnanameDB = generatedKeys.getString("surname");
                int idDB = generatedKeys.getInt("id");
//                System.out.println(nameDB);

                Darbuotojas darbinis = new Darbuotojas(idDB, nameDB, surnanameDB);
                temp.add(darbinis);
            }

            return temp;


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return temp;
    }

    public static Darbuotojas getDarbuotojas(int id) {

        Darbuotojas temp = null;
        String firstName = "";
        String lastName = "";
        try {
            String query = "SELECT * FROM `darbuotojai` WHERE id =" + id;
            Statement st = databaseConnection.createStatement();

            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                firstName = rs.getString("name");
                lastName = rs.getString("surname");
                System.out.println(firstName + " " + lastName + " id: " + id);

            }

            temp = new Darbuotojas(firstName, lastName);

            st.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return temp;
    }


    private void saveToDatabase() {
        try {
            String insertQueryStatement = "INSERT INTO `darbuotojai` (`name`, `surname`, `gender`, `phone`, `birthday`, `education`, `salary`, `pareigos`) VALUES (?, ?, ?, ?,?, ?, ?, ?)";
            databasePrepareStatement = databaseConnection.prepareStatement(insertQueryStatement, Statement.RETURN_GENERATED_KEYS);
            databasePrepareStatement.setString(1, this.name);
            databasePrepareStatement.setString(2, this.surname);
            databasePrepareStatement.setString(3, this.gender);
            databasePrepareStatement.setString(4, this.phone);
            databasePrepareStatement.setString(5, this.birthday);
            databasePrepareStatement.setString(6, this.education);
            databasePrepareStatement.setDouble(7, this.salary);
            databasePrepareStatement.setString(8, this.pareigos);
            // execute insert SQL statement
            databasePrepareStatement.executeUpdate();

            try (ResultSet generatedKeys = databasePrepareStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    System.out.println(generatedKeys.getInt(1));
                    this.id = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
            log(this.id + " " + this.name + " " + this.surname + " added successfully");
        } catch (
                SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateInDatabase() {
        try {
            String insertQueryStatement = "UPDATE `darbuotojai` SET `name` = ?, `surname` = ?, `gender` = ? , `phone` = ?, `birthday` = ?, `education` = ?, `salary` = ?, `pareigos` = ? WHERE id = ?";
            databasePrepareStatement = databaseConnection.prepareStatement(insertQueryStatement);
            databasePrepareStatement.setString(1, this.name);
            databasePrepareStatement.setString(2, this.surname);
            databasePrepareStatement.setString(3, this.gender);
            databasePrepareStatement.setString(4, this.phone);
            databasePrepareStatement.setString(5, this.birthday);
            databasePrepareStatement.setString(6, this.education);
            databasePrepareStatement.setDouble(7, this.salary);
            databasePrepareStatement.setString(8, this.pareigos);
            databasePrepareStatement.setInt(9, this.id);
            // execute insert SQL statement
            databasePrepareStatement.executeUpdate();
            log(this.name + " " + this.surname + " updated successfully");
        } catch (SQLException e) {
            System.out.print("Kazkas blogai");
            e.printStackTrace();
        }
    }

}
